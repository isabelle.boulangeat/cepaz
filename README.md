Reading data
------------

Tous les relevés phyto du CBNA après 2013 (incluant les zones CEPAZ et
les autres) sont chargés.

La table des Zones Pastorales avec infos conplémentaires est chargée.

La liste de taxons du CBNA relevés après 2013 avec le statut des
espèces, et la liste des espèces de la zp avec leur statuts et autres
stats.

``` r
library(openxlsx)
# datphyto = read.xlsx("releves_phyto_CEPAZ_2013+.xlsx")
# head(datphyto,3)

library(rgdal)
```

    ## Loading required package: sp

    ## rgdal: version: 1.5-12, (SVN revision 1018)
    ## Geospatial Data Abstraction Library extensions to R successfully loaded
    ## Loaded GDAL runtime: GDAL 3.1.1, released 2020/06/22
    ## Path to GDAL shared files: /Library/Frameworks/R.framework/Versions/4.0/Resources/library/rgdal/gdal
    ## GDAL binary built with GEOS: TRUE 
    ## Loaded PROJ runtime: Rel. 6.3.1, February 10th, 2020, [PJ_VERSION: 631]
    ## Path to PROJ shared files: /Library/Frameworks/R.framework/Versions/4.0/Resources/library/rgdal/proj
    ## Linking to sp version:1.4-2
    ## To mute warnings of possible GDAL/OSR exportToProj4() degradation,
    ## use options("rgdal_show_exportToProj4_warnings"="none") before loading rgdal.

``` r
datphyto = readOGR(dsn = "releves_phyto_cbna_sup2013_v3.shp", layer = "releves_phyto_cbna_sup2013_v3")
```

    ## OGR data source with driver: ESRI Shapefile 
    ## Source: "/Users/isabelleboulangeat/Documents/PROJETS/(CEPAZ)/cepaz-git/releves_phyto_cbna_sup2013_v3.shp", layer: "releves_phyto_cbna_sup2013_v3"
    ## with 355035 features
    ## It has 52 fields
    ## Integer64 fields read as strings:  rel_pente 2DHA2 2DHAIV 2DHAV 5PRNA1 5PRNA2 6PRPAC 6PRRH 7PR01 7PR04 7PR05 7PR38 7PR74

``` r
# nb 355 035 releves-especes
head(datphyto@data,3)
```

    ##   numchrono     date libsource codeobs1               libobs1 codeinsee
    ## 1   1540150 20190614 CBN Alpin      JCV VILLARET Jean-Charles     38052
    ## 2   1540150 20190614 CBN Alpin      JCV VILLARET Jean-Charles     38052
    ## 3   1540124 20190614 CBN Alpin      JCV VILLARET Jean-Charles     38052
    ##           libcomune codesurfac            libsurface rel_surfac libgeologi
    ## 1 Le Bourg-d'Oisans          2 de 11 \303\240 100 m2        100       <NA>
    ## 2 Le Bourg-d'Oisans          2 de 11 \303\240 100 m2        100       <NA>
    ## 3 Le Bourg-d'Oisans          2 de 11 \303\240 100 m2         30       <NA>
    ##   altinf altisup codeexposi libexposit codepente
    ## 1    860     870       <NA>       <NA>         3
    ## 2    860     870       <NA>       <NA>         3
    ## 3    850     860       <NA>       <NA>         4
    ##                            libpente rel_pente
    ## 1 de 27\302\260 \303\240 45\302\260        30
    ## 2 de 27\302\260 \303\240 45\302\260        30
    ## 3 de 45\302\260 \303\240 70\302\260        60
    ##                                                                                                                                                                                                                                                                                 rel_milieu
    ## 1                     Boisement clair de Quercus petraea ou de l'hybride Quercus petraea x pubescens avec Tilia platyphyllos et en sous-\303\251tage avec quelques Acer monspessulanum, \303\240 sous-bois essentiellement domin\303\251 par la liti\303\250re de feuilles mortes avec ...
    ## 2                     Boisement clair de Quercus petraea ou de l'hybride Quercus petraea x pubescens avec Tilia platyphyllos et en sous-\303\251tage avec quelques Acer monspessulanum, \303\240 sous-bois essentiellement domin\303\251 par la liti\303\250re de feuilles mortes avec ...
    ## 3 Pelouse rupicole x\303\251rophile et acidiphile \303\240 Festuca acuminata avec Brachypodium rupestre et Bromus erectus, \303\251tablie sur des vires et corniches rocheuses fortement inclin\303\251es et localement colonis\303\251es par des massifs de frutic\303\251e arbustive ...
    ##   idreleve_f codestrate   libstrate precouvres hauteurstr numtaxon
    ## 1    7480675         03 S/Arbustive         10          0       13
    ## 2    7480669         02   Arbustive         20          0       13
    ## 3    7480184         02   Arbustive          5          0       13
    ##               nom_comple taxref_cd_                   nom_comp00 abondance
    ## 1 Acer monspessulanum L.      79763 Acer monspessulanum L., 1753         +
    ## 2 Acer monspessulanum L.      79763 Acer monspessulanum L., 1753         1
    ## 3 Acer monspessulanum L.      79763 Acer monspessulanum L., 1753         1
    ##   code_cotat codestra00                                    libetatstr
    ## 1         LC         09 Observation valeur par d\303\251faut (saisie)
    ## 2         LC         09 Observation valeur par d\303\251faut (saisie)
    ## 3         LC         09 Observation valeur par d\303\251faut (saisie)
    ##   typezoneca libzonecar codeloc_et longl93fic latl93fich lmprecisio CODE X2DHA2
    ## 1          P     Points         10   940521.1    6439791          5 <NA>      0
    ## 2          P     Points         10   940521.1    6439791          5 <NA>      0
    ## 3          P     Points         10   940542.9    6439745          5 <NA>      0
    ##   X2DHAIV X2DHAV X5PRNA1 X5PRNA2 X6PRPAC X6PRRH X7PR01 X7PR04 X7PR05 X7PR38
    ## 1       0      0       0       0       0      0      0      0      0      0
    ## 2       0      0       0       0       0      0      0      0      0      0
    ## 3       0      0       0       0       0      0      0      0      0      0
    ##   X7PR74 dep
    ## 1      0  38
    ## 2      0  38
    ## 3      0  38

``` r
colnames(datphyto@data)
```

    ##  [1] "numchrono"  "date"       "libsource"  "codeobs1"   "libobs1"   
    ##  [6] "codeinsee"  "libcomune"  "codesurfac" "libsurface" "rel_surfac"
    ## [11] "libgeologi" "altinf"     "altisup"    "codeexposi" "libexposit"
    ## [16] "codepente"  "libpente"   "rel_pente"  "rel_milieu" "idreleve_f"
    ## [21] "codestrate" "libstrate"  "precouvres" "hauteurstr" "numtaxon"  
    ## [26] "nom_comple" "taxref_cd_" "nom_comp00" "abondance"  "code_cotat"
    ## [31] "codestra00" "libetatstr" "typezoneca" "libzonecar" "codeloc_et"
    ## [36] "longl93fic" "latl93fich" "lmprecisio" "CODE"       "X2DHA2"    
    ## [41] "X2DHAIV"    "X2DHAV"     "X5PRNA1"    "X5PRNA2"    "X6PRPAC"   
    ## [46] "X6PRRH"     "X7PR01"     "X7PR04"     "X7PR05"     "X7PR38"    
    ## [51] "X7PR74"     "dep"

``` r
head(datphyto@data$CODE,100) # code ZP ou NA
```

    ##   [1] NA          NA          NA          "ZP7308001" NA          NA         
    ##   [7] NA          NA          NA          NA          NA          NA         
    ##  [13] "ZP0419204" "ZP0417802" NA          NA          NA          NA         
    ##  [19] NA          NA          NA          NA          NA          NA         
    ##  [25] NA          NA          NA          "ZP0416706" NA          NA         
    ##  [31] NA          NA          "ZP0500502" NA          NA          NA         
    ##  [37] NA          NA          NA          "ZP8415101" "ZP8415101" "ZP8415101"
    ##  [43] "ZP8415101" NA          NA          NA          NA          NA         
    ##  [49] NA          NA          NA          NA          NA          NA         
    ##  [55] NA          NA          NA          NA          NA          NA         
    ##  [61] NA          NA          NA          NA          NA          NA         
    ##  [67] NA          NA          NA          NA          NA          NA         
    ##  [73] NA          NA          NA          NA          NA          NA         
    ##  [79] NA          NA          NA          NA          NA          NA         
    ##  [85] NA          NA          NA          NA          NA          NA         
    ##  [91] NA          NA          NA          NA          NA          NA         
    ##  [97] NA          NA          NA          NA

``` r
insideZP = datphyto@data[which(!is.na(datphyto$CODE)),]
dim(insideZP)
```

    ## [1] 21237    52

``` r
# polyZone = readOGR("CEPAZ_Zone_Etude_et_ZP", "CEPAZ_Zones_Pastorales")
# head(polyZone@data,3)
# nrow(polyZone@data[which(polyZone$INSEEDEP %in% c("01", "74", "73", "38", "26", "05", "04")),])
#
tabZP = read.csv2("ZP_table.csv", encoding = "UTF-8")
head(tabZP,3)
```

    ##   X.U.FEFF.CODE INSEEDEP INSEECOM        NOM1   NOM2 SURFACE SURF_MNT ETAGE_ALT
    ## 1     ZP0405001        4     4050  LES MAYOLS MAYOLS  221.82   228.56        PM
    ## 2     ZP0405002        4     4050  LES JAUMES         403.12   425.98        PM
    ## 3     ZP0405701        4     4057 CLOT GARCIN         143.60   153.11        PM
    ##   PROP_TYPE1 PROP_REG USAGE MOTIF TRAITE TRANSFORM PHAE MAE SURF_MAE ANNEE_REF
    ## 1        COM        N     O            N         N    N   N        0      2012
    ## 2        COM        N     O            N         N    N   N        0      2012
    ## 3        COM        N     O            N         N    N   N        0      2012
    ##   SOURCE ID_SOURCE AUT_SOURCE PROP_TYPE2 PROP_TYPE3 DERN_USAGE EXPLOIT TYPE1
    ## 1 CERPAM                             PRI          N         NA       1    OA
    ## 2 CERPAM                             PRI          N         NA       1    EQ
    ## 3 CERPAM                             PRI          N         NA       1    OA
    ##   TYPE2 TYPE3 PRINTEMPS ETE AUTOMNE HIVER EF_OV_15J EF_CA_15J EF_VL_15J
    ## 1                     O   O       O     N       250         0         0
    ## 2                     O   O       O     O         0         0         0
    ## 3                     O   O       O     O       300         0         0
    ##   EF_AB_15J EF_EQ_15J CH_MAX_OV CH_MAX_CA CH_MAX_VL CH_MAX_AB CH_MAX_EQ DFCI
    ## 1         0         0       250         0         0         0         0    N
    ## 2         0        15         0         0         0         0        15    N
    ## 3         0         0       300         0         0         0         0    N
    ##   MILIEU ENQUETEUR ORIG_FID
    ## 1      L        DB        0
    ## 2      L        DB        0
    ## 3      D        DB        0

``` r
colnames(tabZP)[1] = "CODE"
nrow(tabZP)
```

    ## [1] 5084

``` r
statuts_all = read.xlsx("taxon_cbna_2013+.xlsx")
head(statuts_all,3)
```

    ##   numtaxon             Taxoncbna_nom_complet CD_NOM.(Taxref12)
    ## 1    17343                 Abies alba Miller             79319
    ## 2    17345          Abies cephalonica Loudon             79325
    ## 3    17350 Abies nordmanniana (Steven) Spach             79345
    ##                    Nom_complet.(Taxref12) CD_REF.(Taxref12) 2DHA2 2DHAIV 2DHAV
    ## 1                  Abies alba Mill., 1768             79319    NA     NA    NA
    ## 2          Abies cephalonica Loudon, 1838             79325    NA     NA    NA
    ## 3 Abies nordmanniana (Steven) Spach, 1841             79345    NA     NA    NA
    ##   5PRNA1 5PRNA2 6PRPACA 6PRRH 7PR01 7PR04 7PR05 7PR38 7PR74 UICN_PACA UICN_RA
    ## 1     NA     NA      NA    NA    NA    NA    NA    NA    NA        LC      LC
    ## 2     NA     NA      NA    NA    NA    NA    NA    NA    NA      <NA>    <NA>
    ## 3     NA     NA      NA    NA    NA    NA    NA    NA    NA      <NA>    <NA>

``` r
nrow(statuts_all)
```

    ## [1] 5606

``` r
statuts_all$UICN = statuts_all$UICN_PACA
statuts_all$UICN[which(is.na(statuts_all$UICN_PACA))] = statuts_all$UICN_RA[which(is.na(statuts_all$UICN_PACA))]

sp_zp = read.xlsx("stat_taxons_zp_statuts.xlsx")
head(sp_zp)
```

    ##   numtaxon                                                             nom_cbna
    ## 1    40496                                              Acer campestre L., 1753
    ## 2    10912                                               Agrimonia eupatoria L.
    ## 3    15467                             Anthoxanthum odoratum L. subsp. odoratum
    ## 4     6684                                              Anthyllis vulneraria L.
    ## 5     4948                                            Arenaria serpyllifolia L.
    ## 6    15482 Arrhenatherum elatius (L.) P. Beauv. ex J. & C. Presl subsp. elatius
    ##    cd_nom                                                         nom_comple
    ## 1   79734                                            Acer campestre L., 1753
    ## 2   80410                                       Agrimonia eupatoria L., 1753
    ## 3  131447                     Anthoxanthum odoratum subsp. odoratum L., 1753
    ## 4   82999                                      Anthyllis vulneraria L., 1753
    ## 5 9999993                                            Zz Attente rattachement
    ## 6  131693 Arrhenatherum elatius (L.) P.Beauv. ex J. & C.Presl subsp. elatius
    ##    cd_ref                                                          nom_valid
    ## 1   79734                                            Acer campestre L., 1753
    ## 2   80410                                       Agrimonia eupatoria L., 1753
    ## 3   82922                                     Anthoxanthum odoratum L., 1753
    ## 4   82999                                      Anthyllis vulneraria L., 1753
    ## 5 9999993                                            Zz Attente rattachement
    ## 6  131693 Arrhenatherum elatius (L.) P.Beauv. ex J. & C.Presl subsp. elatius
    ##        CODE dep reg _min _max _count DHA2 DHAIV DHAV PRNA1 PRNA2 PRPAC PRRH
    ## 1 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ## 2 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ## 3 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ## 4 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ## 5 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ## 6 ZP0123901  01  RA 2014 2014      1   NA    NA   NA    NA    NA    NA   NA
    ##   PR01 PR04 PR05 PR38 PR74 lr_fr lr_paca lr_ra
    ## 1   NA   NA   NA   NA   NA    LC    <NA>    LC
    ## 2   NA   NA   NA   NA   NA    LC    <NA>    LC
    ## 3   NA   NA   NA   NA   NA    LC    <NA>    LC
    ## 4   NA   NA   NA   NA   NA    LC    <NA>    LC
    ## 5   NA   NA   NA   NA   NA  <NA>    <NA>  <NA>
    ## 6   NA   NA   NA   NA   NA    LC    <NA>  <NA>

Taille des ZP représentées
--------------------------

``` r
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(ggplot2)

# taille des ZP représentées
tabZP_phyto = merge(tabZP, insideZP, by = "CODE", all=FALSE)
tabZP_phyto$dataset = "phyto"

tabZP$dataset = "all"
selectCol = c("CODE", "dataset", "SURFACE")
rbind(tabZP_phyto[, selectCol], tabZP[, selectCol]) %>%
  ggplot(aes(SURFACE, fill=dataset, colour = dataset, stat(density))) +
  geom_histogram(alpha=0.2, position = "identity")
```

    ## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.

![](exploration_files/figure-markdown_github/ZP_size-1.png)

On a une sous-représentation des ZP de petite taille.

Taille des ZP par departement
-----------------------------

``` r
selectCol = c("CODE", "dep", "SURFACE")
tabZP_phyto[, selectCol] %>%
  ggplot(aes(SURFACE, fill=dep, colour = dep, stat(density))) +
  geom_histogram(alpha=0.2, position = "identity")
```

    ## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.

![](exploration_files/figure-markdown_github/ZP_size_dep-1.png)

Taille des ZP par milieu
------------------------

``` r
selectCol = c("CODE", "MILIEU", "SURFACE")
tabZP_phyto[, selectCol] %>%
  ggplot(aes(SURFACE, fill=MILIEU, colour = MILIEU, stat(density))) +
  geom_histogram(alpha=0.2, position = "identity")
```

    ## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.

![](exploration_files/figure-markdown_github/ZP_size_milieu-1.png)

Nb de relevés par ZP
--------------------

``` r
# nb ZP où il y a (au moins) un releve phyto
length(unique(insideZP$CODE))
```

    ## [1] 172

``` r
length(unique(tabZP$CODE))
```

    ## [1] 5084

``` r
# nombre de releve phyto par ZP
zp_releve = insideZP %>% group_by(CODE) %>% summarise(n = length(unique(numchrono)))
```

    ## `summarise()` ungrouping output (override with `.groups` argument)

``` r
summary(zp_releve$n)
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   1.000   1.000   2.000   4.529   5.000  69.000

``` r
zp = readOGR("Extract_raster_zp(20_02_2020)", "CEPAZ_ZP")
```

    ## OGR data source with driver: ESRI Shapefile 
    ## Source: "/Users/isabelleboulangeat/Documents/PROJETS/(CEPAZ)/cepaz-git/Extract_raster_zp(20_02_2020)", layer: "CEPAZ_ZP"
    ## with 5084 features
    ## It has 48 fields
    ## Integer64 fields read as strings:  Join_Count TARGET_FID ORIG_FID

``` r
head(zp@data)
```

    ##   Join_Count TARGET_FID      CODE INSEEDEP INSEECOM        NOM1   NOM2 SURFACE
    ## 0          1          0 ZP0405001       04    04050  LES MAYOLS MAYOLS  221.82
    ## 1          1          1 ZP0405002       04    04050  LES JAUMES   <NA>  403.12
    ## 2          1          2 ZP0405701       04    04057 CLOT GARCIN   <NA>  143.60
    ## 3          1          3 ZP0405702       04    04057  LES GRAVES   <NA>  129.17
    ## 4          1          4 ZP0405703       04    04057        BANE   <NA>   65.45
    ## 5          1          5 ZP0405704       04    04057      ROUAST   <NA>   29.38
    ##   SURF_MNT ETAGE_ALT PROP_TYPE1 PROP_REG USAGE                     MOTIF TRAITE
    ## 0   228.56        PM        COM        N     O                      <NA>      N
    ## 1   425.98        PM        COM        N     O                      <NA>      N
    ## 2   153.11        PM        COM        N     O                      <NA>      N
    ## 3   135.37        PM        COM        N     O                      <NA>      N
    ## 4    70.25        PM        COM        N     N probl\303\250mes fonciers      N
    ## 5    30.97        PM        COM        N     O                      <NA>      N
    ##   TRANSFORM PHAE MAE SURF_MAE ANNEE_REF SOURCE ID_SOURCE AUT_SOURCE PROP_TYPE2
    ## 0         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ## 1         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ## 2         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ## 3         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ## 4         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ## 5         N    N   N        0      2012 CERPAM      <NA>       <NA>        PRI
    ##   PROP_TYPE3 DERN_USAGE EXPLOIT TYPE1 TYPE2 TYPE3 PRINTEMPS  ETE AUTOMNE HIVER
    ## 0          N       <NA>       1    OA  <NA>  <NA>         O    O       O     N
    ## 1          N       <NA>       1    EQ  <NA>  <NA>         O    O       O     O
    ## 2          N       <NA>       1    OA  <NA>  <NA>         O    O       O     O
    ## 3          N       <NA>       0  <NA>  <NA>  <NA>      <NA> <NA>    <NA>  <NA>
    ## 4          N       2009       0  <NA>  <NA>  <NA>      <NA> <NA>    <NA>  <NA>
    ## 5          N       <NA>       1    AB  <NA>  <NA>         O    O       N     N
    ##   EF_OV_15J EF_CA_15J EF_VL_15J EF_AB_15J EF_EQ_15J CH_MAX_OV CH_MAX_CA
    ## 0       250         0         0         0         0       250         0
    ## 1         0         0         0         0        15         0         0
    ## 2       300         0         0         0         0       300         0
    ## 3         0         0         0         0         0         0         0
    ## 4         0         0         0         0         0         0         0
    ## 5        25         0         0         0         0         0         0
    ##   CH_MAX_VL CH_MAX_AB CH_MAX_EQ DFCI MILIEU ENQUETEUR ORIG_FID
    ## 0         0         0         0    N      L        DB        0
    ## 1         0         0        15    N      L        DB        0
    ## 2         0         0         0    N      D        DB        0
    ## 3         0         0         0    N      H        DB        0
    ## 4         0         0         0    N      L        DB        0
    ## 5         0         0         0    N      H        DB        0

``` r
zp_n = merge(zp, zp_releve, by = "CODE")
zp_n$CBNA = ifelse(is.na(zp_n$n), 0, 1)
```

Nb de ZP dans territoire agrement CBNA
--------------------------------------

``` r
CBNA = readOGR(".", "CBNA")
```

    ## OGR data source with driver: ESRI Shapefile 
    ## Source: "/Users/isabelleboulangeat/Documents/PROJETS/(CEPAZ)/cepaz-git", layer: "CBNA"
    ## with 1 features
    ## It has 11 fields
    ## Integer64 fields read as strings:  ID_GEOFLA

``` r
CBNA_proj = spTransform(CBNA, CRS("+init=epsg:4326"))

library(leaflet)
zp_n_proj <- spTransform(zp_n, CRS("+init=epsg:4326")) # Reproject coordinates
qpal <- colorBin(c("red", "blue"), zp_n_proj$CBNA, bins=3)
# qpal <- colorQuantile("Greens", zp_n_proj$n, n = 2, reverse = TRUE)

# leaflet(zp_n_proj) %>%
#   addPolygons(stroke = TRUE,opacity = 1,fillOpacity = 0.5, smoothFactor = 0.5, color=NA,fillColor = ~qpal(CBNA),weight = 1,) %>%
#   addLegend(values=~CBNA,pal=qpal, labels = c("aucune", "au moins une"), labFormat = "factor", title="observations CBNA") %>%
#    addProviderTiles("CartoDB.Positron") %>%
#    addPolygons(data = CBNA_proj, fill = F, weight = 2, color = "black")

table(zp_n$CBNA)
```

    ## 
    ##    0    1 
    ## 4912  172

``` r
library(rgeos)
```

    ## rgeos version: 0.5-3, (SVN revision 634)
    ##  GEOS runtime version: 3.8.1-CAPI-1.13.3 
    ##  Linking to sp version: 1.4-2 
    ##  Polygon checking: TRUE

``` r
index = lapply(1:nrow(zp_n_proj), function(x){gWithin(zp_n_proj[x,], CBNA_proj)})
sum(unlist(index))
```

    ## [1] 3948

``` r
sum(unlist(index)) / nrow(zp_n_proj)
```

    ## [1] 0.7765539

``` r
table(zp_n$CBNA, zp_n$INSEEDEP)
```

    ##    
    ##      01  04  05  06  13  26  38  73  74  83  84
    ##   0   7 922 542 220 217 972 291 304 769 369 299
    ##   1   1  42  17   0   0  36  27  35  13   0   1

``` r
table(zp_n$CBNA, zp_n$MILIEU)
```

    ##    
    ##        B    D    H    L
    ##   0 1536  293 1882 1175
    ##   1   39   25   73   35

``` r
# > table(zp_n$CBNA, zp_n$INSEEDEP)
#
#      01  04  05  06  13  26  38  73  74  83  84
#   0   7 922 542 220 217 972 291 304 769 369 299
#   1   1  42  17   0   0  36  27  35  13   0   1
# > table(zp_n$CBNA, zp_n$MILIEU)
#
#        B    D    H    L
#   0 1536  293 1882 1175
#   1   39   25   73   35
```

Diversité alpha des ZP
======================

``` r
# nombre d'especes par ZP, pour releves phyto
zp_espece_phyto = insideZP %>% group_by(numchrono) %>% summarise(sp_richness_phyto = length(unique(numtaxon)))
```

    ## `summarise()` ungrouping output (override with `.groups` argument)

``` r
div_alpha_zp = unique(merge(insideZP[, c("CODE", "numchrono")], zp_espece_phyto, by = "numchrono"))
head(div_alpha_zp)
```

    ##    numchrono      CODE sp_richness_phyto
    ## 1     936131 ZP0420911                14
    ## 15    936132 ZP0420911                17
    ## 32    936133 ZP0420911                11
    ## 43    936134 ZP0420911                17
    ## 60    936135 ZP0420911                12
    ## 72    936143 ZP0420911                12

``` r
summary(div_alpha_zp)
```

    ##    numchrono           CODE           sp_richness_phyto
    ##  Min.   : 936131   Length:779         Min.   : 1.00    
    ##  1st Qu.: 973367   Class :character   1st Qu.:17.00    
    ##  Median :1143734   Mode  :character   Median :26.00    
    ##  Mean   :1183099                      Mean   :26.94    
    ##  3rd Qu.:1360846                      3rd Qu.:35.00    
    ##  Max.   :1552452                      Max.   :87.00

``` r
stat_alpha = div_alpha_zp %>% group_by(CODE) %>% summarise(alpha_mean = mean(sp_richness_phyto), nb_rel = length(unique(numchrono)))
```

    ## `summarise()` ungrouping output (override with `.groups` argument)

``` r
summary(stat_alpha)
```

    ##      CODE             alpha_mean        nb_rel      
    ##  Length:172         Min.   : 3.00   Min.   : 1.000  
    ##  Class :character   1st Qu.:18.88   1st Qu.: 1.000  
    ##  Mode  :character   Median :26.14   Median : 2.000  
    ##                     Mean   :27.21   Mean   : 4.529  
    ##                     3rd Qu.:33.54   3rd Qu.: 5.000  
    ##                     Max.   :66.00   Max.   :69.000

``` r
# richesse des releves phyto hors ZP (ou total)
div_alpha_tot = datphyto@data %>% group_by(numchrono) %>% summarise(sp_richness_phyto = length(unique(numtaxon)))
```

    ## `summarise()` ungrouping output (override with `.groups` argument)

``` r
summary(div_alpha_tot)
```

    ##    numchrono       sp_richness_phyto
    ##  Min.   : 930251   Min.   : 1.0     
    ##  1st Qu.: 973228   1st Qu.:12.0     
    ##  Median :1199246   Median :20.0     
    ##  Mean   :1211180   Mean   :20.9     
    ##  3rd Qu.:1448651   3rd Qu.:28.0     
    ##  Max.   :1552558   Max.   :88.0

``` r
# detail par milieu et departement (figure)
stat_alpha_details = unique(merge(stat_alpha, tabZP, by = "CODE"))
head(stat_alpha_details)
```

    ##        CODE alpha_mean nb_rel INSEEDEP INSEECOM                       NOM1
    ## 1 ZP0400802      64.00      1        4     4008                LES GASTRES
    ## 2 ZP0401801      42.75      4        4     4018              PIED D ENROUX
    ## 3 ZP0401804       8.50      4        4     4018       LE PETIT TOURTOUILLE
    ## 4 ZP0402003       9.20      5        4     4020 LE CHATEAU-VAULX-DESCOURES
    ## 5 ZP0402004      21.50      2        4     4020                TOURTOUREAU
    ## 6 ZP0402301      16.50      2        4     4023                   LE SEUIL
    ##                   NOM2 SURFACE SURF_MNT ETAGE_ALT PROP_TYPE1 PROP_REG USAGE
    ## 1                       179.26   211.72        MM        PRI        N     O
    ## 2               ENROUX  212.13   219.07        PM        PRI        N     O
    ## 3 LE PETIT TOURTOUILLE   43.54    44.77        PM        PRI        N     N
    ## 4                       355.52   394.64        MM        PRI        N     O
    ## 5                       112.55   131.94        MM        PRI        N     N
    ## 6               REBEIE  569.22   636.92        MM        COM        N     O
    ##              MOTIF TRAITE TRANSFORM PHAE MAE SURF_MAE ANNEE_REF SOURCE
    ## 1                       O         O    N   N        0      2012 CERPAM
    ## 2                       N         N    N   N        0      2013 CERPAM
    ## 3   ARRET ACTIVITE      N         N    N   N        0      2013 CERPAM
    ## 4                       N         N    N   N        0      2012 CERPAM
    ## 5 TROP DE LOUPS!!!                     N   N        0      2012 CERPAM
    ## 6                       N         N    N   N        0      2012 CERPAM
    ##   ID_SOURCE AUT_SOURCE PROP_TYPE2 PROP_TYPE3 DERN_USAGE EXPLOIT TYPE1 TYPE2
    ## 1                             COM          N         NA       1    VL    CL
    ## 2                               N          N         NA       1    OA     N
    ## 3                               N          N       2013       1    CL     N
    ## 4                             COM          N         NA       4    OA    EQ
    ## 5                             COM          N       2008       0            
    ## 6                               N          N         NA       3    AB    OA
    ##   TYPE3 PRINTEMPS ETE AUTOMNE HIVER EF_OV_15J EF_CA_15J EF_VL_15J EF_AB_15J
    ## 1     N         O   O       O     O         0        80        10         0
    ## 2     N         N   O       N     N       420         0         0         0
    ## 3     N         O   O       O     O         0        30         0         0
    ## 4     N         O   N       O     N         0         0         0         0
    ## 5                                           0         0         0         0
    ## 6               O   N       O     N         0         0         0         0
    ##   EF_EQ_15J CH_MAX_OV CH_MAX_CA CH_MAX_VL CH_MAX_AB CH_MAX_EQ DFCI MILIEU
    ## 1         0         0        80        10         0         0    N      B
    ## 2         0       420         0         0         0         0    N      B
    ## 3         0         0        30         0         0         0    N      B
    ## 4         0      2000         0         0         0         5    N      L
    ## 5         0         0         0         0         0         0    N      H
    ## 6         0       500         0         0        60         0    N      D
    ##   ENQUETEUR ORIG_FID dataset
    ## 1        SG        0     all
    ## 2        PG        0     all
    ## 3        PG        0     all
    ## 4        DB        0     all
    ## 5        DB        0     all
    ## 6        DB        0     all

``` r
stat_alpha_details %>% group_by(MILIEU) %>%
  ggplot(aes(MILIEU, alpha_mean)) +
  geom_boxplot(notch=TRUE) +
  stat_boxplot(na.rm=TRUE) +
  ylim(0,100) +
  facet_wrap(~INSEEDEP)
```

    ## notch went outside hinges. Try setting notch=FALSE.

    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.
    ## notch went outside hinges. Try setting notch=FALSE.

![](exploration_files/figure-markdown_github/diversite_alpha-1.png)

Statut des espèces des ZP
=========================

Nombre et proportion d’espèces liste rouge parmi les espèces relevées
Rappel: Eteinte (EX), Eteinte à l’état sauvage (EW), En danger critique
(CR), En danger (EN), Vulnérable (VU), Quasi menacée (NT), Préoccupation
mineure (LC), Données insuffisantes (DD), Non évaluée (NE).

Evalué avec les données “points” comprenant les relevés phyto et des
relevés supplémentaires d’espèces cible.

``` r
sp_phyto = unique(datphyto@data$numtaxon)
length(sp_phyto) # 3789 especes
```

    ## [1] 3789

``` r
sp_phyto_zp = unique(insideZP$numtaxon)
length(sp_phyto_zp) # 1587 especes
```

    ## [1] 1587

``` r
sp_points = unique(statuts_all$numtaxon) # après 2013
length(sp_points) # 5606
```

    ## [1] 5606

``` r
sp_zp_2013 = sp_zp[which(sp_zp$`_max`>=2013),]
sp_points_zp = unique(sp_zp_2013$numtaxon)
length(sp_points_zp) # 2146
```

    ## [1] 2146

``` r
length(unique(sp_zp_2013$CODE))
```

    ## [1] 742

``` r
statuts_all$UICN =as.factor(statuts_all$UICN)
levels(statuts_all$UICN) = c("CR","CR","DD","EN","EW","LC", "NE"  ,"NT",  "RE",  "VU")
table(unique(statuts_all[, c("numtaxon", "UICN")])$UICN)
```

    ## 
    ##   CR   DD   EN   EW   LC   NE   NT   RE   VU 
    ##   53  433  116    2 2431  186  173   13  164

``` r
aa = table(unique(statuts_all[, c("numtaxon", "UICN")])$UICN) / length(sp_points)
sum(is.na(unique(statuts_all[, c("numtaxon", "UICN")])$UICN))/ length(sp_points)
```

    ## [1] 0.3630039

``` r
aa
```

    ## 
    ##           CR           DD           EN           EW           LC           NE 
    ## 0.0094541563 0.0772386729 0.0206921156 0.0003567606 0.4336425259 0.0331787371 
    ##           NT           RE           VU 
    ## 0.0308597931 0.0023189440 0.0292543703

``` r
sp_zp_2013$UICN =as.factor(sp_zp_2013$lr_fr)

table(unique(sp_zp_2013[, c("numtaxon", "UICN")])$UICN)
```

    ## 
    ##   DD   EN   LC   NT   VU 
    ##   33    4 1628   28    4

``` r
zp = table(unique(sp_zp_2013[, c("numtaxon", "UICN")])$UICN) / length(sp_points_zp)
sum(is.na(unique(sp_zp_2013[, c("numtaxon", "UICN")])$UICN))/ length(sp_points_zp)
```

    ## [1] 0.2176142

``` r
zp_vec = unclass(zp)
zp
```

    ## 
    ##          DD          EN          LC          NT          VU 
    ## 0.015377446 0.001863933 0.758620690 0.013047530 0.001863933

``` r
library(reshape2)
rbind(unclass(aa),c(CR=0, zp_vec[1:2], EW=0, zp_vec[3], NE=0, zp_vec[4], RE=0, zp_vec[5])) %>% melt() %>%
  ggplot(aes(x = Var2, y = value, fill = factor(Var1))) +
  geom_bar(stat = "identity", position = position_dodge2()) +
  scale_x_discrete(name="statut UICN") +
  scale_y_continuous(name = "proportion de taxons") +
  labs(fill = "emprise") +
  scale_fill_hue(labels = c("CBNA", "ZP"))
```

![](exploration_files/figure-markdown_github/status_especes-1.png) On
retrouve 38% de la flore de la zone d’agrément du CBNA échantillonnée
dans les ZP.

Attention pour les statuts il manque 36% de données dans la liste
complète et 22% dans les espèces des ZP.

Attention, toute cette évaluation exclue les départements hors zone CBNA
(au Sud).
